# Build step demo

A simple demo of building a model leading up to bringing the tool into a serverless environment. The overall idea behind this is to present a high-level demonstration of steps to bring basic models to deployment.

This is a cross-posted with an earlier blog-entry.